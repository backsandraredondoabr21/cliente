package net.techu.data;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface ClienteRepository extends MongoRepository<ClienteMongo, String> {

    //private List<ClienteMongo> dataList = new ArrayList<ClienteMongo>();
    @Query("{'nombre':?0}")
    public Optional<ClienteMongo> findByNombre(String nombre);

    @Query("{'nombre':?0}")
    public Optional<ClienteMongo> deleteByNombre(String nombre);
}
